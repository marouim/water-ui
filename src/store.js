import Vue from 'vue'
import Vuex from 'vuex'
import waterApi from './api/waterApi'

Vue.use(Vuex)

const READ_MOISTURE_VALUES_SUCCESSFUL = 'READ_MOISTURE_VALUES_SUCCESSFUL'
//const READ_MOISTURE_VALUES_FAILED = 'READ_MOISTURE_VALUES_FAILED'


const state = {
  moistureArray: null
}

const mutations = {
  [READ_MOISTURE_VALUES_SUCCESSFUL] (state, data) {
    state.moistureArray = data
  }
}

const actions = {
  getMoistureValues({ commit }) {
    waterApi.getMoistureValues()
      .then((response) => {
        commit(READ_MOISTURE_VALUES_SUCCESSFUL, response.data)
      })
  }
}

const getters = {
  moistureValues() {
    return state.moistureArray
  }
}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
