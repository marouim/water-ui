import Vue from 'vue'
import { createOidcAuth, SignInType, LogLevel } from 'vue-oidc-client'

const loco = window.location
const appRootUrl = `${loco.protocol}//${loco.host}${process.env.BASE_URL}`

var mainOidc = createOidcAuth(
  'main',
  SignInType.Window,
  appRootUrl,
  {
    authority: 'https://sso-secureapp.apps.acocp.rhcasalab.com/auth/realms/water/protocol/openid-connect/auth',
    realm: 'water',
    client_id: 'water-ui',
    response_type: 'id_token token',
    scope: 'openid profile email api',
    prompt: 'login',
    login_hint: 'bob'
  },
  console,
  LogLevel.Debug
)
Vue.prototype.$oidc = mainOidc
export default mainOidc
