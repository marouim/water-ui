import Vue from 'vue';
import VueRouter from 'vue-router'

// Import router components
import Dashboard from './components/Dashboard.vue'
import Zones from './components/Zones.vue'

Vue.use(VueRouter)

const routes = [
  { 
    name: 'dashboard',
    path: '/', 
    component: Dashboard,
    meta: {
      requiresAuth: true
    }
  },
  {
    name: 'zones',
    path: '/zones',
    component: Zones 
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (router.app.$keycloak.authenticated) {
      next()
    } else {
      const loginUrl = router.app.$keycloak.createLoginUrl()
      window.location.replace(loginUrl)
    }
  } else {
    next()
  }
})

export default router;