import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";
import store from "./store";
import axios from "axios";

import VueKeycloakJs from "@dsb-norge/vue-keycloak-js";

Vue.config.productionTip = false;

function tokenInterceptor() {
  axios.interceptors.request.use(
    (config) => {
      config.headers.Authorization = `Bearer ${Vue.prototype.$keycloak.token}`;
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
}

Vue.use(VueKeycloakJs, {
  init: {
    // Use 'login-required' to always require authentication
    // If using 'login-required', there is no need for the router guards in router.js
    onLoad: "check-sso",
  },
  config: {
    url: "https://sso-sso.apps.openshift.rhcasalab.com/auth",
    clientId: "water-ui",
    realm: "water",
  },
  onReady: () => {
    tokenInterceptor();
    new Vue({
      vuetify,
      router,
      store,
      render: (h) => h(App),
    }).$mount("#app");
  },
});
