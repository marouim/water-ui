import axios from 'axios'

const api = {
    baseUrl: '/api',
    customerListUrl: '/values',

    getMoistureValues() {
        return axios({
            method: 'GET',
            baseURL: this.baseUrl,
            url: this.customerListUrl
        })
    }
}

export default api