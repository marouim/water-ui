module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    proxy: {
      "/api": {
        target: "https://water.apps.acocp.rhcasalab.com",
        logLevel: "debug",
        changeOrigin: true,
        secure: false
      }
    }
  }
}